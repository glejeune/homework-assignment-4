/*
 * @author: Gregoire Lejeune
 */

// Imports
const http = require('http');
const https = require('https');
const fs = require('fs');
const path = require('path');
const config = require('../app/config');
const router = require('./router');
const logger = require('./logger');

module.exports = {
  init() {
    http.createServer(router).listen(config.httpPort, () => {
      logger.info(`The HTTP server is running on port ${config.httpPort}`);
    });

    // Create and start HTTPS server
    const httpsServerOptions = {
      key: fs.readFileSync(path.join(__dirname, '..', 'https', 'localhost.key')),
      cert: fs.readFileSync(path.join(__dirname, '..', 'https', 'localhost.crt')),
    };
    https
      .createServer(httpsServerOptions, router)
      .listen(config.httpsPort, () => {
        logger.info(`The HTTPS server is running on port ${config.httpsPort}`);
      });
  },
};
