const readline = require('readline');
const util = require('util');
const debug = util.debuglog('cli');
const events = require('events');
const logger = require('./logger');
const menuItem = require('../app/models/menu-item');
const order = require('../app/models/order');
const user = require('../app/models/user');

class _events extends events {}
const e = new _events();

const cli = {
  hr() {
    console.log('-'.repeat(process.stdout.columns));
  },

  nl(n = 1) {
    process.stdout.write('\n'.repeat(n));
  },

  center(str) {
    const leftPadding = Math.floor((process.stdout.columns - str.length) / 2);
    console.log('%s%s', ' '.repeat(leftPadding), str);
  },
};

const responder = {
  help() {
    const commands = {
      exit: 'Kill the CLI (and the rest of the application)',
      man: 'Show this help page',
      help: 'Alias of the "man" command',
      menu: 'View all the current menu items',
      'list orders':
        'View all the recent orders in the system (orders placed in the last 24 hours)',
      'more order info --{orderId}': 'Lookup the details of a specific order',
      'list users':
        'View all the users who have signed up in the last 24 hours',
      'more user info --{email}': 'Lookup the details of a specific user',
    };
    const maxCommandLength = Object.keys(commands).reduce(
      (acc, key) => (acc < key.length ? key.length : acc),
      0,
    );
    cli.hr();
    cli.center('CLI MANUAL');
    cli.hr();
    Object.keys(commands).forEach(key => {
      const padRight = maxCommandLength - key.length;
      console.log(
        '\x1b[33m%s\x1b[0m%s - %s',
        key,
        ' '.repeat(padRight),
        commands[key],
      );
    });
    cli.nl();
  },

  exit() {
    process.exit(0);
  },

  menu() {
    menuItem.find((err, menuObjectList) => {
      if (!err && menuObjectList && menuObjectList.length > 0) {
        console.dir(menuObjectList, {color: true});
        cli.nl();
      }
    });
  },

  listOrders() {
    order.find((err, orderObjectList) => {
      if (!err && orderObjectList && orderObjectList.length > 0) {
        orderObjectList.forEach(orderObject => {
          if (
            (Date.now() - new Date(`${orderObject.date}`).getTime()) /
              (1000 * 60 * 60 * 24) <=
            1.0
          ) {
            console.log(orderObject.id);
          }
        });
        cli.nl();
      }
    });
  },

  moreOrderInfo(str) {
    let [_, orderId] = str.split('--');
    orderId = orderId.trim().length > 0 ? orderId.trim() : false;
    if (orderId) {
      order.findByID(orderId, (err, orderObject) => {
        if (!err && orderObject) {
          console.dir(orderObject, {color: true});
          cli.nl();
        }
      });
    }
  },

  listUsers() {
    user.find((err, userObjectList) => {
      if (!err && userObjectList && userObjectList.length > 0) {
        userObjectList.forEach(userObject => {
          if (
            (Date.now() - new Date(`${userObject.createdAt}`).getTime()) /
              (1000 * 60 * 60 * 24) <=
            1.0
          ) {
            console.log(userObject.email);
          }
        });
        cli.nl();
      }
    });
  },

  moreUserInfo(str) {
    let [_, userEmail] = str.split('--');
    userEmail = userEmail.trim().length > 0 ? userEmail.trim() : false;
    if (userEmail) {
      user.findByEmail(userEmail, (err, userObject) => {
        if (!err && userObject) {
          console.dir(userObject, {color: true});
          cli.nl();
        }
      });
    }
  },
};

e.on('man', _ => {
  responder.help();
});

e.on('help', _ => {
  responder.help();
});

e.on('exit', _ => {
  responder.exit();
});

e.on('menu', _ => {
  responder.menu();
});

e.on('list orders', _ => {
  responder.listOrders();
});

e.on('more order info', str => {
  responder.moreOrderInfo(str);
});

e.on('list users', _ => {
  responder.listUsers();
});

e.on('more user info', str => {
  responder.moreUserInfo(str);
});

module.exports = {
  processCmd(str) {
    str = typeof str == 'string' && str.trim().length > 0 ? str.trim() : false;
    if (str) {
      const uniqueInputs = [
        'man',
        'help',
        'exit',
        'menu',
        'list orders',
        'more order info',
        'list users',
        'more user info',
      ];

      var matchFound = false;
      uniqueInputs.some(function(input) {
        if (str.toLowerCase().indexOf(input) > -1) {
          matchFound = true;
          e.emit(input, str);
          return true;
        }
      });

      if (!matchFound) {
        console.log('Invalid command. See `help`.');
      }
    }
  },

  init() {
    logger.info(`The CLI is running`);

    const _interface = readline.createInterface({
      input: process.stdin,
      output: process.stdout,
      prompt: '',
    });

    _interface.prompt();

    _interface.on('line', str => {
      this.processCmd(str);
      _interface.prompt();
    });

    _interface.on('close', () => {
      responder.exit();
    });
  },
};
