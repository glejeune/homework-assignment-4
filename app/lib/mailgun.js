const https = require('https');
const querystring = require('querystring');

const config = require('../config');

module.exports = {
  send(to, subject, text, callback) {
    const payload = querystring.stringify({
      from: `${config.mailgun.from}@${config.mailgun.domain}`,
      to,
      subject,
      text,
    });

    const requestDetails = {
      protocol: 'https:',
      hostname: 'api.mailgun.net',
      method: 'POST',
      path: `/v3/${config.mailgun.domain}/messages`,
      auth: `api:${config.mailgun.privateKey}`,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Content-Length': Buffer.byteLength(payload),
      },
    };

    const req = https.request(requestDetails, res => {
      const status = res.statusCode;
      if (status === 200) {
        callback(false);
      } else {
        callback({error: `Stripe charge failed with status ${status}`});
      }
    });

    req.on('error', function(e) {
      callback(e);
    });

    req.write(payload);

    req.end();
  },
};
