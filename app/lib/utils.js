const crypto = require('crypto');
const config = require('../config');

module.exports = {
  createRandomString(strLength) {
    if (typeof strLength !== 'number' || strLength <= 0) return false;

    if (strLength) {
      const possibleCharacters = 'abcdefghijklmnopqrstuvwxyz0123456789';
      let str = '';
      for (let i = 1; i <= strLength; i++) {
        str += possibleCharacters.charAt(
          Math.floor(Math.random() * possibleCharacters.length),
        );
      }
      return str;
    } else {
      return false;
    }
  },

  hash(string) {
    if (typeof string === 'string' && string.length > 0) {
      const hash = crypto
        .createHmac('sha256', config.hashingSecret)
        .update(string)
        .digest('hex');
      return hash;
    } else {
      return false;
    }
  },
};
