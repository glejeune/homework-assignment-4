const controller = require('../../lib/controller');
const template = require('../../lib/template');

module.exports = controller('/settings', {
  get(data, callback) {
    template.render('settings.html', {head: {title: 'Settings'}}, callback);
  },
});
