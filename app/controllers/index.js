const controller = require('../../lib/controller');
const template = require('../../lib/template');

module.exports = controller('/', {
  get(data, callback) {
    template.render('index.html', {head: {title: 'Menu'}}, callback);
  },
});
