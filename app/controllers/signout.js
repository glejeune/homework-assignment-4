const controller = require('../../lib/controller');
const template = require('../../lib/template');

module.exports = controller('/signout', {
  get(data, callback) {
    template.render('signout.html', {head: {title: 'Sign out'}}, callback);
  },
});
