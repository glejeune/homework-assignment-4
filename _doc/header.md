Homework Assignment #4 for [The Node.js Master Class](https://pirple.thinkific.com/courses/the-nodejs-master-class).

# <a name="assignment"></a> Assignment

It is time to build the Admin CLI for the pizza-delivery app you built in the previous assignments. Please build a CLI interface that would allow the manager of the pizza place to:

1. View all the current menu items
2. View all the recent orders in the system (orders placed in the last 24 hours)
3. Lookup the details of a specific order by order ID
4. View all the users who have signed up in the last 24 hours
5. Lookup the details of a specific user by email address

This is an open-ended assignment. You can take any direction you'd like to go with it, as long as your project includes the requirements. It can include anything else you wish as well. 


# <a name="configuration"></a> Configuration

Before running the server, you must edit the configuration in `app/config.js`. In this file edit the values for keys :

* `environment.[staging, production].mailgun.domain` : Your Mailgun domain.
* `environment.[staging, production].mailgun.privateKey` : Your Mailgun private key.
* `environment.[staging, production].mailgun.from` : The `name` of the sender. Thus the sender email will be `mailgun.from`@`mailgun.domain`.
* `environment.[staging, production].stripe.secretKey` : Your Stripe private key.

# <a name="run"></a> Run

```
git clone https://gitlab.com/glejeune/homework-assignment-4
cd homework-assignment-4
node index.js
```

Then go to http://localhost:3000

# <a name="cli"></a> CLI

Available CLI commands:

* `exit`: Kill the CLI (and the rest of the application).
* `man`: Show this help page.
* `help`: Alias of the "man" command.
* `menu`: View all the current menu items.
* `list orders`: View all the recent orders in the system (orders placed in the last 24 hours).
* `more order info --{orderId}`: Lookup the details of a specific order.
* `list users`: View all the users who have signed up in the last 24 hours.
* `more user info --{email}`: Lookup the details of a specific user.

# <a name="makefile"></a> Makefile

This project comes with a `Makefile` with the following rules:

* `default`: Start the server.
* `doc`: Generate the README.md file with API documentation.
* `clean`: Remove generated files (https certificats and keys) and application datas (`.data` directory)
